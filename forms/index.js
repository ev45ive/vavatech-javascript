
const deliveryBtn = document.getElementById('js-btn-delivery')
const pickupBtn = document.getElementById('js-btn-pickup')

const deliverySection = document.getElementById('section_delivery')
const pickupSection = document.getElementById('section_pickup')

const shops = [
    {
        id: '123',
        name: 'Sklep ul. Sezamkowa',
        city: 'Warszawa'
    },
    {
        id: '234',
        name: 'Sklep Galeria Zlote tarasy',
        city: 'Warszawa'
    },
    {
        id: '345',
        name: 'Sklep ul. Grunwaldzka',
        city: 'Poznań'
    },
    {
        id: '456',
        name: 'Sklep Galeria Avenida',
        city: 'Poznań'
    },
]

const order = {
    method: 'delivery',
    pickup: { shop: null },
    delivery: {
        formData: {}
    },
    invoice: {
        formData: {}
    }
}

setupDeliveryChoiceSection()

const validators = {
    zipcode: function (el) {
        const ok = el.value.match(/^\d{2,2}\-\d{3,3}$/) !== null
        return ok ? '' : 'Must be in format XX-XXX'
    }
}

setupFormElements(document.getElementById('delivery_form'), validators, function (data) {
    order.delivery.formData = data
    console.log('form chagned', data);
    if(document.getElementById('delivery_form').checkValidity()){
        document.getElementById('section_invoice').classList.add('show')
    }
})

setupFormElements(document.getElementById('invoice_form'), validators, function (data) {
    order.invoice.formData = data
    console.log('form chagned', data);
})


function setupDeliveryChoiceSection() {

    updateSections()
    buildShopSelector(shops)


    deliveryBtn.addEventListener('click', function () {
        order.method = 'delivery'
        updateSections()
    })

    pickupBtn.addEventListener('click', function () {
        order.method = 'pickup'
        updateSections()
    })

    function updateSections() {
        deliverySection.classList.toggle('show', order.method === 'delivery')
        pickupSection.classList.toggle('show', order.method === 'pickup')
    }
}


function buildShopSelector(shops) {
    const select_city = document.getElementById('select_city')
    const select_shop = document.getElementById('select_shop')

    const shopsByCity = shops.reduce(function (groups, shop) {
        groups[shop.city] = groups[shop.city] || []
        groups[shop.city].push(shop)
        return groups;
    }, {})
    const cities = Object.keys(shopsByCity)

    select_city.innerHTML = '<option value=""> -- Select Shop -- </option>'
    cities.forEach(function (city) {
        option = document.createElement('option')
        option.value = city
        option.innerText = city
        select_city.append(option)
    })
    select_shop.parentElement.classList.remove('show')

    select_city.addEventListener('change', function () {
        const selected_city = select_city[select_city.selectedIndex].value
        const city_shops = shopsByCity[selected_city]

        select_shop.innerHTML = '<option value=""> -- Select Shop -- </option>'
        select_shop.parentElement.classList.add('show')

        city_shops.forEach(function (shop) {
            // option = document.createElement('option')
            // option.value = shop.id
            // option.innerText = shop.name
            select_shop.append(new Option(shop.name, shop.id))
        })
    })

    select_shop.addEventListener('change', function (event) {
        const selectedId = event.target[event.target.selectedIndex].value
        const shop = shops.find(function (shop) { return shop.id === selectedId })
        order.pickup.shop = (shop);
    })
}

function setupFormElements(formEl, validators = {}, callback) {
    const data = {}

    for (let name in formEl.elements) {
        if (!formEl.elements.hasOwnProperty(name)) continue;
        if (parseInt(name) >= 0) continue;
        const elem = formEl.elements[name]
        data[name] =  elem.type == 'checkbox' ?
            elem.checked : elem.value


        elem.addEventListener('blur', function (event) {
            const el = event.target
            event.target.parentElement.classList.add('was-validated')

            const result = validateElement(validators, el)
            callback(data)
        })
        elem.addEventListener('input', function (event) {
            const el = event.target
            data[event.target.name] =
                event.target.type == 'checkbox' ?
                    event.target.checked : event.target.value

            console.log(order)
            console.log(event.target.validationMessage)

            event.target.parentElement.classList.add('was-validated')

            const result = validateElement(validators, el)
            callback(data)
        })
    }
}

function validateElement(validators, el) {
    const validator = validators[el.name]

    if (validator) {
        el.setCustomValidity(validator(el) || '')
    }
    if (el.validationMessage)
        el.parentElement.querySelector('.invalid-feedback')
            .innerText = el.validationMessage

    return el.validationMessage
}
