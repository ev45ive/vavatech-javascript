const assert = console.assert;

function setup(items = [], products) {
    const cart = new Cart(items);
    products = products || [
        { id: '123', price: 100 },
        { id: '234', price: 50 },
    ];
    return { cart, products };
}


(function () {
    // costam = setup()
    // cart = costam.cart
    // products = costam.products
    const { cart, products } = setup();

    console.log('Empty cart')
    assert(cart.getItems().length == 0, 'Initial cart not empty')
    cart.addProduct(products[0])

    console.log('Adding first product')
    assert(cart.getItems().length == 1, 'Product not in cart')
    assert(cart.getItemById('123').count == 1, 'Product count is wrong')
    assert(cart.getItemById('123').total == 100, 'Item subtotal is wrong')
    assert(cart.getTotal() == 100, 'Total is wrong')

    console.log('Adding second product')
    cart.addProduct(products[0])
    assert(cart.getItemById('123').count == 2, 'Product count is wrong')
    assert(cart.getItemById('123').total == 200, 'Item subtotal is wrong')
    assert(cart.getTotal() == 200, 'Total is wrong')

})();

(function () {
    const { cart, products } = setup();

    console.log('Empty cart')
    assert(cart.getItems().length == 0, 'Initial cart not empty')
    console.log('Adding 2 x same product')
    cart.addProduct(products[0])
    cart.addProduct(products[0])

    console.log('Removing one product')
    cart.removeProduct('123')

    assert(cart.getItemById('123').count == 1, 'Product count is wrong')
    assert(cart.getItemById('123').total == 100, 'Item subtotal is wrong')
    assert(cart.getTotal() == 100, 'Total is wrong')

    console.log('Removing last product')
    cart.removeProduct('123')
    assert(cart.getItemById('123') == undefined, 'Product is still in cart')
    assert(cart.getTotal() == 0, 'Total is wrong')

})()
