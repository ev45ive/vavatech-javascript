/* this._cartItems = [
    // {
    //     id: products[0].id,
    //     product: products[0],
    //     count: 2,
    //     price: products[0].price,
    //     total: products[0].price * 2
    // }
] */

function Cart(initialItems = []) {
    this._cartItems = initialItems
    this._cartMeta = {
        count: 2,
        total: 19.94
    }

    this.updateTotals()
}

Object.assign(Cart.prototype, {
    getItems() {
        return this._cartItems
    },
    addProduct(product) {
        let cartItem = this.getItemById(product.id)

        if (!cartItem) {
            cartItem = cartItem || {
                id: product.id,
                product: product,
                price: product.price,
                count: 0,
                total: 0
            }
            // this._cartItems.push(cartItem)
            this._cartItems = [...this._cartItems, cartItem]
        }

        this.updateProductCount(cartItem.id, cartItem.count + 1)
    },
    removeProduct(id) {
        let cartItem = this.getItemById(id)

        if (!cartItem) return
        this.updateProductCount(id, cartItem.count - 1)
    },
    updateProductCount(id, count) {
        this._cartItems = this._cartItems.filter(function (cartItem) {
            // Different item -> leave
            if (cartItem.id !== id) return true

            // Same id -> Update item
            cartItem.count = count
            cartItem.price = cartItem.product.price
            cartItem.total = cartItem.price * cartItem.count

            // no items - remove
            return (cartItem.count > 0)
        })
        this.updateTotals()
    },
    updateTotals() {
        this._cartMeta = this._cartItems.reduce(function (meta, item) {
            meta.count += item.count
            meta.total += item.total
            return meta
        }, {
            count: 0,
            total: 0
        })
    },
    getItemById(id) {
        return this._cartItems.find(function (item) {
            return item.id === id
        })
    },
    getTotal() {
        return this._cartMeta.total
    }

})