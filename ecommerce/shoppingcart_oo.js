
// cart = new Cart()
// cart.getItems()
// cart.addProduct(products[1])

class EventEmitter {
    _listeners = {}
    addEventListener(type, fn) {
        this._listeners[type] = this._listeners[type] || new Map()
        this._listeners[type].set(fn, fn)
    }
    removeEventListener(type, fn) {
        // if(this._listeners[type]) this._listeners[type].delete(fn)
        // this._listeners[type] && this._listeners[type].delete(fn)
        // this._listeners[type]? this._listeners[type].delete(fn) : undefined
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Optional_chaining
        this._listeners[type]?.delete(fn)
    }
    dispatchEvent(event) {
        this._listeners[event.type]?.forEach(fn => fn(event))
    }
}

class CartItemUpdatedEvent {
    constructor(type, target) {
        this.type = (type)
        this.target = target
    }
}

class Product {
    id = ''
    name = ''
}

// new CartItem( ...
// CartItem(product: Product, count?: number): CartItem
// Create new cart line item from product

class CartItem extends EventEmitter {

    static createEmpty(product) {
        return new CartItem(product, 0)
    }

    /**
     * Create new cart line item from product
     * @param {Product} product 
     * @param {number} count 
     */
    constructor(product, count = 1) {
        super()
        // if (!product instanceof Product)
        //     throw new Error('Invalid product')

        this.id = product.id;
        this.product = product
        this.count = count;
        this.total = product.price
        this._calculateSubtotal()
    }

    _calculateSubtotal() {
        // if (typeof this.count !== 'number' || this.count < 0) {
        //     throw new Error('Invalid product count')
        // }
        this.total = this.product.price * this.count
    }

    increment() {
        this.count++
        this._calculateSubtotal()
        this.dispatchEvent(new CartItemUpdatedEvent('increment', this))
    }

    decrement() {
        this.count--
        this._calculateSubtotal()
        this.dispatchEvent(new CartItemUpdatedEvent('decrement', this))
    }
}

class Cart extends EventEmitter {

    constructor(items = []) {
        super()
        this._items = items
        this._total = 0
    }

    getItems() {
        return this._items
    }

    addProduct(product) {
        let item = this.getItemById(product.id)

        if (!item) {
            // item = new CartItem(product, 0)
            item = CartItem.createEmpty(product)
            this._items.push(item)
            item.addEventListener('increment', (event) => {
                this._updateTotal()

                this.dispatchEvent(event)
            })
            item.addEventListener('decrement', (event) => {
                this._updateTotal()

                if (event.target.count > 0) {
                    this.dispatchEvent(event)
                    return
                }
                const index = this._items.indexOf(event.target)
                if (index !== -1) {
                    this._items.splice(index, 1)

                    this.dispatchEvent({ type: 'removed', target: item })
                }

            })
            this.dispatchEvent({ type: 'added', target: item })
        }
        item.increment()
    }

    getItemById(id) {
        return this._items.find(i => i.id === id)
    }

    _updateTotal() {
        this._total = this._items.reduce((total, item) => total += item.total, 0)
    }
    getTotal() {
        return this._total
    }

    removeProduct(id) {
        const item = this.getItemById(id)
        item.decrement()
    }
}