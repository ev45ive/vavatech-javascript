const pageTitleEl = document.querySelector('h1')
pageTitleEl.innerText = "Products"


const showOnlyPromotedBtn = document.querySelector('#showOnlyPromotedBtn')
const showAllBtn = document.querySelector('#showAllBtn')
const limitInputEl = document.querySelector('#limitInputEl')

limitInputEl.addEventListener('input', function (event) {
    limit = Math.abs(parseInt(event.target.value)) || 1
    limitInputEl.value = limit

    productsList(products, limit, only_promoted)
})

showOnlyPromotedBtn.addEventListener('click', function () {
    only_promoted = true
    productsList(products, limit, only_promoted)
})

showAllBtn.addEventListener('click', function () {
    only_promoted = undefined
    productsList(products, limit, only_promoted)
})


tax = 0.23
globalDiscount = 0.5

var limit = 3
var only_promoted = undefined

var renderProduct = renderProductDOM
// var renderProduct = renderProductConsole

productsList()

/// =========



listElem.addEventListener('click', addToCartClick)

function addToCartClick(event) {
    if (!event.target.matches('.js-add-to-cart')) { return }
    const parent = event.target.closest('[data-product-id]')
    const productId = parent.dataset.productId
    const product = getProductById(productId)

    console.log(product);
}

const cart = new Cart()