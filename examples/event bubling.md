## event bubbling

```html
<div id="container">
    <div class="list-group-item">
         <div id="child"> <!-- click here --> </div> 
```

```js
child.onclick = (event) => {
  event.target == child;
  event.currentTarget == child
};
```


```js
container.onclick = (event) => {
  event.target == child;
  event.currentTarget == container;

  event.target.matches('.list-group-item') // false
  
  event.target.closest('.list-group-item') === <div class="list-group-item">
};
```

