## callback hell
```js

echo('Hello ', res => {
    echo(res + 'Alice ', res => {
        echo(res + 'and her cat ', render)
    })
})

```

```js
echo = (msg) => {
    return new Promise((resolveCb)=>{
      setTimeout(()=>{
          resolveCb(msg)
      },2000)
    })  
}

render = result => console.log(result)

promise = echo('Hello ')

// p2 = promise.then( res => res + 'Alice' )  
// promise.then( res => echo(res + 'Alice').then(... ) 

// p2 = promise.then( res => echo(res + 'Alice ') )  
// p3 = p2.then( res => echo(res + 'and her Promises ') )  
// p3.then(render)

echo('Hello ')
.then( res => echo(res + 'Alice ') )  
.then( res => echo(res + 'and her Promises ') )  
.then( render )


```

## Splitting / sharing promise chain
```js

p = echo('Hello ')
.then( res => echo(res + 'Alice ') )  

p.then( res => echo(res + 'and her Promises ') )  
.then( render )

p
.then( res => echo(res + 'and her Promises ') )
.then( res => echo(res + 'and more Promises ') )  
.then( render )

```


## Eror handling
```js


echo = (msg, err) => {
    return new Promise((resolveCb, rejectCb)=>{
      setTimeout(()=>{
          err? rejectCb(err) : resolveCb(msg)
      },2000)
    })  
}

render = result => console.log(result)


p = echo('Hello ',' upss.. ')
.then( res => echo(res + 'Alice '), err => echo('Anonymous ') )  

p.then( res => echo(res + 'and her Promises ') )  
.then( render )

p
// .then( res => echo(res + 'and her Promises ', 'upss 2..') )
.then( res => echo(res + 'and her Promises ', 'upss 2..') )
.then( res => echo(res + 'and more Promises ', err => Promise.reject('No luck') ) )  
// .catch( err => 'Unexpected error' )
.then( render )
.finally(() => console.log('completed') )


```

## ASync await
```js

echo = (msg, err) => {
    return new Promise((resolveCb, rejectCb)=>{
      setTimeout(()=>{
          err? rejectCb(err) : resolveCb(msg)
      },2000)
    })  
}

render = result => console.log(result)

p = ( async () => {
    try{
        res = await echo('Hello ')
        res = await echo(res + 'Alice ')
        res = await echo(res + 'and her Promises ')

        return res; 
    }catch(err){
        return 'Unexpected error'
    }
}) ();

p.then(render)

```