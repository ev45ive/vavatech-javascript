<!-- https://dev.to/jmmedina00/mapping-reducing-and-filtering-in-php-2dc -->

```js
list2 = [];
list = [1, 2, 3, 4, 5];

// for(let i of list){ list2.push(i * 2) }
// for(let i of list){ doubleAndSave(i * 2) }

function doubleAndSave(item) {
  list2.push(item * 2);
}

list.forEach(doubleAndSave);
```

```js
list = [1, 2, 3, 4, 5];

// for(let i of list){ list2.push(i * 2) }

list2 = list.map(function (item) {
  return item * 2;
});

list2; // modified copy
// (5) [2, 4, 6, 8, 10]

list; // original - no changes
// (5) [1, 2, 3, 4, 5]
```

```js
list = [1, 2, 3, 4, 5];
list2 = list.map(function (item, index, all) {
  console.log(item, index, all);
  return item * 2;
});
// VM30891:4 1 0 (5) [1, 2, 3, 4, 5]
// VM30891:4 2 1 (5) [1, 2, 3, 4, 5]
// VM30891:4 3 2 (5) [1, 2, 3, 4, 5]
// VM30891:4 4 3 (5) [1, 2, 3, 4, 5]
// VM30891:4 5 4 (5) [1, 2, 3, 4, 5]
list2;
// (5) [2, 4, 6, 8, 10]
```

```js
list = [1, 2, 3, 4, 5];
list2 = list.filter(function (item, index, all) {
  return item % 2 === 0;
});
list2;
// (2) [2, 4]

list = [1, 2, 3, 4, 5];
list2 = list.filter(function (item, index, all) {
  return item % 2 !== 0;
});
list2;
// (3) [1, 3, 5]
```

```js
list = [1, 2, 3, 4, 5];
total = list.reduce(function (sum, item, index, all) {
  console.log(sum, item, sum + item);
  return sum + item;
}, 0);
// 0 1 1
// 1 2 3
// 3 3 6
// 6 4 10
// 10 5 15

total;
// 15
```

```js
cartItems.find(function (item) {
  console.log(item.id);
  return item.id === "345";
});
//  123
//  345
// > {id: "345", product: {…}, count: 1, price: 19.97, total: 19.97}
```

```js
isEven = function (x) {
  return x % 2 === 0;
};
// isOdd = function(x){ return x % 2 !== 0 };

// Functional Combinator
isNot = function (fn) {
  return function (x) {
    return !fn(x);
  };
};

isOdd = isNot(isEven);

[1, 2, 3, 4, 5].filter(isOdd);
```

```js
isEven = (x) => x % 2 === 0;
// isOdd = x => x % 2 !== 0;

// functional combinator
// isNot = fn => {
//     return (x) => {
//             return !fn(x)
//     }
// }

// partial application / currying
isNot = (fn) => (x) => !fn(x);

isOdd = isNot(isEven);

[1, 2, 3, 4, 5].filter(isOdd);
// (3) [1, 3, 5]
```

https://randycoulman.com/blog/categories/thinking-in-ramda/

## Partial Application

```js
// imperative
request = (token, method, url, resource, id, params) => {
  return `${method} ${url}/${resource}${id ? "/" + id : ""}${
    params ? "?" + params : ""
  }`;
};
request("123234", "GET", "localhost", "users", "123");

// objectr
request = new XMLHttpRequest();
request.open("GET", "localhost/users/123");
request.addEventListener("loadend", UserView.handleLoadEnd);
request.send(null);

// functional partial application
request = (token) => (method) => (url) => (resource) => (id) => () => {
  return `${method} ${url}/${resource}${id ? "/" + id : ""}`;
};
session = request("secrettoken");

get = session("GET");
getLocal = get("localhost");
getUsers = getLocal("users");
refreshUser = getUsers(123);

refreshUser();
```

## Pointfree

```js
const forever21 = ifElse(gte(__, 21), always(21), inc);

const forever21 = (age) => (age > 21 ? 21 : age + 1);

```

## Libraries
https://www.youtube.com/watch?v=m3svKOdZijA&app=desktop&ab_channel=InfoQ
https://fr.umio.us/favoring-curry/

https://github.com/ramda/ramda-fantasy
https://github.com/fantasyland/fantasy-land#monad
https://ramdajs.com/0.21.0/docs/#transduce
https://github.com/lodash/lodash/wiki/FP-Guide


