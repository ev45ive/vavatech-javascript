```js
var global = 'global';

debugger;

function outer(){
    var closure = 'closure';
    global; // global
    debugger;

    function inner(){
        global; // global
        closure // closure
        var local = 'local';

        debugger;
    }
    inner()

// local; // local is not defined

}
outer()
// closure; // local is not defined

```