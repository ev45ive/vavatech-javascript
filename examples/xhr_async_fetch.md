## XHR API

```js
xhr = new XMLHttpRequest();
xhr.open("GET", "products.json");
xhr.onloadend = function (event) {
  const results = JSON.parse(xhr.responseText);
  productsList(results, 10);
};
// --
xhr.send(null);
```

```js
function fetchJSON(url, callback, errback) {
  const xhr = new XMLHttpRequest();
  xhr.open("GET", url);
  xhr.onloadend = function (event) {
    const results = JSON.parse(xhr.responseText);
    callback(results);
  };
  xhr.onerror = errback || console.error;
  xhr.send(null);
}

fetchJSON("products2.json", function (results) {
  productsList(results, 2);
});
```

## ES2015 Fetch
```js
fetch('products.json').then(function(resp){
    resp.json().then(function(data){
        console.log(data) 
    })
})
```

## Chaining thens

```js
p = fetch('products.json')

p2 = p.then(function(resp){
    return resp.json()
})

p2.then(function(data){
        console.log(data) 
})
```

## Arrow function
```js

fetch('products.json')
.then( resp =>  resp.json() )
.then( (data) => {
    console.log(data) 
})

```


## CORS
Access to fetch at 'http://localhost:8080/products?name=Vavatech&test=123&abc=axc' from origin 'http://localhost:5000' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.


## HTTP Methods
```js
fetch('http://localhost:8080/products?name=Vavatech&test=123&abc=axc',{
    method:'POST', body: JSON.stringify({message:'jakies dane'})
})
.then( res => res.json() )
.then( console.log )

{message: "hello", params: {…}, body: "{\"message\":\"jakies dane\"}"}

```

// https://www.codexworld.com/post-receive-json-data-using-php-curl/


### Form data
```js
form = new FormData()

form.append('daneA','A')
form.append('daneB','B')

fetch('http://localhost:8080/products?name=Vavatech&test=123&abc=axc',{
    method:'POST', body: form, 
//     headers:{
//         'Content-Type':'application/x-www-form-urlencoded'
//     }
})
.then( res => res.json() )
.then( console.log )
```


## Callbacks
```js
echo = (msg, cb) => {
    setTimeout(()=>{
        cb(msg)
    },2000)
}

render = result => console.log(result)

echo('Hello', render)

// VM16123:7 Hello

```

## callback hell / callback pyramid of doom

```js
echo = (msg, cb) => {
    setTimeout(()=>{
        cb(msg)
    },2000)
}

render = result => console.log(result)

echo('Hello ', res => {
    echo(res + 'Alice ', res => {
        echo(res + 'and her cat ', render)
    })
})

// Hello Alice and her cat 

```

<!-- fetch('http://localhost/')
// .then( res => res.json() )
.then( console.log ) -->
localhost/:1
 Access to fetch at 'http://localhost/' from origin 'http://localhost:5000' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.
VM88:1 GET http://localhost/ net::ERR_FAILED