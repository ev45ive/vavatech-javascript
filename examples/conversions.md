```js
12 - {};
// NaN
NaN;
// NaN
NaN + 12;
// NaN
NaN / 2;
// NaN
NaN === 12;
// false
NaN == NaN;
// false
isNaN(NaN);
// true
```

```js
Infinity -
  // Infinity
  Infinity;
// -Infinity
Infinity == Infinity;
// true
0 / 0;
// NaN
1 / 0;
// Infinity

isFinite(Infinity);
// false
```

````js
String
// ƒ String() { [native code] }
Number
// ƒ Number() { [native code] }
Object
// ƒ Object() { [native code] }
Array
// ƒ Array() { [native code] }
Function
ƒ Function() { [native code] }
typeof function(){}
// "function"
Function('x+1')
// ƒ anonymous(
// ) {
// x+1
// }
Date
// ƒ Date() { [native code] }
Date
// ƒ Date() { [native code] }
Math
// Math {abs: ƒ, acos: ƒ, acosh: ƒ, asin: ƒ, asinh: ƒ, …}
JSON.stringify(product)
// "{\"name\":\"Placki \\\"Choco's\\\"\",\"description\":\"Placki Czekoladowe\",\"price\":9.97,\"promotion\":true}"
JSON.parse("{\"name\":\"Placki \\\"Choco's\\\"\",\"description\":\"Placki Czekoladowe\",\"price\":9.97,\"promotion\":true}")
// {name: "Placki \"Choco's\"", description: "Placki Czekoladowe", price: 9.97, promotion: true}description: "Placki Czekoladowe"name: "Placki \"Choco's\""price: 9.97promotion: true[[Prototype]]: Object
Error
// ƒ Error() { [native code] }
new Error('ups..')
// Error: ups..
//     at <anonymous>:1:1

````

## Conversions

```js
'1'+'1'
// "11"
'1' + '1'
// "11"
12 + '12'
// "1212"
'12' + 12 
// "1212"
'12' - 12 
// 0
```

```js
parseInt('10')
// 10
parseInt('10.12')
// 10
parseInt('10 kg pomidorow')
// 10
parseInt('10 kg pomidorow i 2 kg jablek')
// 10
parseInt('placki, 10 kg pomidorow i 2 kg jablek')
// NaN
parseInt('placki i 2 kg jablek')
// NaN
parseInt('10 000 ')
// 10
parseInt('FF',16)
// 255
parseInt('10000',2)
// 16
parseFloat('100.999')
// 100.999
```

```js
(100).toString(10)
// "100"
(100).toString(16)
// "64"
(100).toString(2)
// "1100100"
(100.3).toString(2)
"1100100.0100110011001100110011001100110011001100110011"
// 0.1 + 0.1
0.2
// 0.1 + 0.2
0.30000000000000004
// parseInt('999999999999999')
999999999999999
// parseInt('9999999999999999')
10000000000000000
// parseInt('9999999999999999999999')
1e+22
```

https://github.com/MikeMcl/bignumber.js/

## Money
store cents (grosze)

var cena = 10.99
var cena = 1099


```js
9.97 * 0.1
// 0.9970000000000001
(19.97 * 0.1).toPrecision(4)
// "1.997"
(19.97 * 0.1).toFixed(2)
// "2.00"
Math.floor(1.5)
// 1
Math.ceil(1.5)
// 2
Math.round(1.5)
// 2
(19.97 * 0.1).toPrecision(3)
// "2.00"
(19.87 * 0.1).toPrecision(3)
// "1.99"
```

## Logic
```js
true && 1 && '1' && 'placki'
// "placki"
false && true && 1 && '1' && 'placki'
// false
'' && true && 1 && '1' && 'placki'
// ""
if('' && true && 1 && '1'){
    x =  'placki'
}
// undefined
x  = '' && true && 1 && '1' && 'placki'
// ""
x  = true && 1 && '1' && 'placki'
// "placki"
```

```js
1 == '1'
// true
1 === '1'
// false
1 !== '1'
// true
undefined === undefined
// true
undefined === null
// false
undefined == null
// true
```

```js
typeof []
// "object"
typeof {} 
// "object"
typeof new Date()
// "object"
[] instanceof Array 
// true
[] instanceof Object 
// true
{} instanceof Object 
// VM15762:1 Uncaught SyntaxError: Unexpected token 'instanceof'
({}) instanceof Object 
// true
(new Date()) instanceof Date
// true
```