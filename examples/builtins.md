```js
var utf8 = 'Gżęgżółką - λ - 😋' 
undefined

utf8.charCodeAt(10)

```


```js
new Number(123)
// Number {123}[[Prototype]]: Number[[PrimitiveValue]]: 123
typeof (123)
// "number"
typeof (123) === 'number'

// true
Number('123')
// 123
String(1123)

// "1123"
Number('123 kg')
// NaN

Number({})
// NaN

String({})
"[object Object]"

(123).toString()
"123"
```