```js

list = [1,2,3,4,5]
list2 = []

for(let index in list){ list2[index] = list[index] * 2 }

list2
// (5) [2, 4, 6, 8, 10]

list2 = []
list = [1,2,3,4,5]
for(let item of list){ list2.push(item * 2) }
list2
// (5) [2, 4, 6, 8, 10]


```