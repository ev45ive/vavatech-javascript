```js

obj1 = {a:1, b:2, c:3};
obj2 = {a:'a',    c:'c', x: 123};

Object.assign({}, obj1, obj2)
// {a: "a", b: 2, c: "c", x: 123}

obj1
// {a: 1, b: 2, c: 3}
obj2 
// {a: "a", c: "c", x: 123}

obj1 = {a:1, b:2, c:3};
obj2 = {a:'a',    c:'c', x: 123};

// Object.assign({}, obj1, obj2)
obj = { ...obj1, ...obj2  } 


{a: "a", b: 2, c: "c", x: 123}
obj1 = {a:1, b:2, c:3, nested:[1,2,3,4]};
obj2 = {a:'a',    c:'c', x: 123};

// Object.assign({}, obj1, obj2)
obj = { ...obj1, ...obj2  } 

// Nested objects - references - shallow copy
{a: "a", b: 2, c: "c", nested: Array(4), x: 123}

obj.nested[2] = 'x'
"x"
obj1.nested
// (4) [1, 2, "x", 4]

obj1 = {a:1, b:2, c:3, nested:[1,2,3,4]};
obj2 = {a:'a',    c:'c', x: 123};

// Object.assign({}, obj1, obj2)
obj = { ...obj1, ...obj2, nested: [ ...obj1.nested ] } 
// {a: "a", b: 2, c: "c", nested: Array(4), x: 123}a: "a"b: 2c: "c"nested: (4) [1, 2, 3, 4]x: 123[[Prototype]]: Object


// Fast deep copy ( data only )
JSON.parse( JSON.stringify(obj1) )
// {a: 1, b: 2, c: 3, nested: Array(4)}

```