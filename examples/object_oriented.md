```js
function makePerson(name) {
  let person = {};

  person.name = name;
  person.sayHello = function () {
    return "Hi, I am " + person.name;
  };

  return person;
}

alice = makePerson("Alice");
// {name: "Alice", sayHello: ƒ}
bob = makePerson("Bob");
// {name: "Bob", sayHello: ƒ}

alice.sayHello();
// "Hi, I am Alice"
bob.sayHello();
// "Hi, I am Bob"
```

```js
alice.sayHello;
// ƒ (){ return 'Hi, I am ' + person.name }
bob.sayHello;
// ƒ (){ return 'Hi, I am ' + person.name }

alice.sayHello == bob.sayHello;
false;
```

```js
function makePerson(name) {
  let person = {};

  person.name = name;
  person.sayHello = sayHello;

  return person;
}

function sayHello() {
  return "Hi, I am " + person.name;
}

alice = makePerson("Alice");
bob = makePerson("Bob");
// alice.sayHello == bob.sayHello
true;
```

```js
function makePerson(name) {
  this.name = name;
  this.sayHello = function () {
    return "Hi, I am " + this.name;
  };
  return this;
}

alice = makePerson("Alice");
bob = makePerson("Bob");

// Window {window: Window, self: Window, document: document, name: "Bob", location: Location, …}
alice;
// Window {window: Window, self: Window, document: document, name: "Bob", location: Location, …}
bob;
// Window {window: Window, self: Window, document: document, name: "Bob", location: Location, …}
```

```js
function Person(name) {
  this.name = name;
  this.sayHello = function () {
    return "Hi, I am " + this.name;
  };
  //     return this
}

pinokio = { note: "I will be a person" };
// Person.apply(pinokio, ['Pinokio'])
Person.call(pinokio, "Pinokio");
pinokio.sayHello();
// "Hi, I am Pinokio"
pinokio;
// {note: "I will be a person", name: "Pinokio", sayHello: ƒ}
```

```js
function Person(name) {
  this.name = name;
  this.sayHello = function () {
    return "Hi, I am " + this.name;
  };
  //     return this
}

pinokio = { __proto__: Person.prototype };
// Person.apply(pinokio, ['Pinokio'])
Person.call(pinokio, "Pinokio");
pinokio;
// Person {name: "Pinokio", sayHello: ƒ}name: "Pinokio"sayHello: ƒ ()[[Prototype]]: Objectconstructor: ƒ Person(name)[[Prototype]]: Object

pinokio instanceof Person;
true;
```

```js
function Person(name) {
  this.name = name;
}
Person.prototype.sayHello = function () {
  return "Hi, I am " + this.name;
};


alice = Object.create(Person.prototype)
// or:
// pinokio = { __proto__: Person.prototype };

Person.call(pinokio, "Pinokio");
pinokio;

// Person {name: "Pinokio"}
//  name: "Pinokio"
//  [[Prototype]]: Person
//     sayHello: ƒ ()
//     constructor: ƒ Person(name)
//     [[Prototype]]: Object

pinokio.sayHello();
// "Hi, I am Pinokio"
Person.prototype.sayHello.apply(pinokio, []);
// "Hi, I am Pinokio"
Person.prototype.lie = function () {
  return "I am a boy";
};
// ƒ (){ return 'I am a boy' }
pinokio.lie();
// "I am a boy"
Person.prototype.lie = function () {
  this.noseSize += 1;
};
pinokio.lie();
// undefined
pinokio;
// Person {name: "Pinokio", noseSize: NaN}
```
## new keyword

```js
function Person(name){
    this.name = name
}
Person.prototype.sayHello = function (){
    return 'Hi, I am ' + this.name
}

// alice = Object.create(Person.prototype)
// pinokio = { __proto__: Person.prototype }
// Person.call(alice, 'Alice')

alice = new Person('Alice')
alice.sayHello()

"Hi, I am Alice"
```

## AngularJS Scopes

```js
function Scope(){
    this.id = Scope.prototype.id++ 
}
Scope.prototype.id = 1;
Scope.prototype.$new = function(){
    let child =  Object.create(this) 
    child.id = Scope.prototype.id++ 
    return child
}
ƒ (){
    let child =  Object.create(this) 
    child.id = Scope.prototype.id++ 
    return child
}

root = new Scope()
// Scope {id: 1}
child1 = root.$new()
// Scope {id: 2}
child1child = child1.$new()
// Scope {id: 3}
child1child2 = child1.$new()
// Scope {id: 4}


//     id: 4
//     [[Prototype]]: Scope
//         id: 2
//         [[Prototype]]: Scope
//             id: 1
//             [[Prototype]]: Object

root.user = 'zbyszek'
// "zbyszek"
child1.user
// "zbyszek"
child1child.user
// "zbyszek"
child1child2.user
// "zbyszek"

child1.post = 'Lubie placki'
// "Lubie placki"
child1child.post
// "Lubie placki"
root.post 
// undefined
child1child2.post = "Suggested article"
"Suggested article"
child1child2
// Scope {id: 4, post: "Suggested article"}
//     id: 4
//     post: "Suggested article"
//         [[Prototype]]: Scope
//         id: 2
//         post: "Lubie placki"
//             [[Prototype]]: Scope
//             id: 1
//             user: "zbyszek"
// [[Prototype]]: Object
```

## Prototypal inherintance

```js
function Person(name){
    this.name = name
//     this.legalAge = 21
}
Person.prototype.sayHello = function (){
    return 'Hi, I am ' + this.name
}

function Employee(name, salary){
    Person.apply(this, arguments)
    this.salary = salary
}
Employee.prototype = Object.create( Person.prototype ) 
Employee.prototype.work = function (){
    return 'I first need my  ' + this.salary
}

alice = new Person('Alice')
tom = new Employee('Tom', 1200)
tom.work()

"I first need my  1200"
tom.sayHello()
"Hi, I am Tom"
tom 
// Employee {name: "Tom", salary: 1200}
//     name: "Tom"
//     salary: 1200
//     [[Prototype]]: Employee 
//         work: ƒ ()
//         [[Prototype]]: Person
//             sayHello: ƒ ()
//             constructor: ƒ Person(name)
//             [[Prototype]]: Object

tom instanceof Employee
// true
tom instanceof Person
// true
```

## DOM Elements as prototype chain

```js
div = document.createElement('div')
<div>​</div>​
div.__proto__
// HTMLDivElement {Symbol(Symbol.toStringTag): "HTMLDivElement", constructor: ƒ}
div.__proto__.__proto__
// HTMLElement {…}
div.__proto__.__proto__.__proto__
// Element {…}
div.__proto__.__proto__.__proto__.__proto__
// Node {…}
div.__proto__.__proto__.__proto__.__proto__.__proto__
// EventTarget {Symbol(Symbol.toStringTag): "EventTarget", addEventListener: ƒ, dispatchEvent: ƒ, removeEventListener: ƒ, constructor: ƒ}
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
// {constructor: ƒ, __defineGetter__: ƒ, __defineSetter__: ƒ, hasOwnProperty: ƒ, __lookupGetter__: ƒ, …}
div.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__.__proto__
null
```

## Mixins

```js
function Logger(log = console.log){
    this.log = log 
}

function Employee(name, salary){
    Person.apply(this, arguments)
    Logger.apply(this,[]) // mixins / traits / aspects
//     Logger.apply(this,[])
    this.salary = salary
}
```

## Es2015 class syntax

```js
class Person{
    constructor(name){
        this.name = name
    }

    sayHello(){
        return 'I am ' + this.name
    }
}
alice = new Person('Alice')
alice.sayHello()

class Employee extends Person{
    
    constructor(name,salary){
        super(name)
        this.salary = salary 
    }
    
    work(){
        return 'I need my ' + this.salary
    }

    sayHello(){
        return super.sayHello() + ' and ' + this.work()
    }
}
tom = new Employee('Alice',1200)
tom.sayHello()
// "I am Alice and I need my 1200"
tom 
// Employee {name: "Alice", salary: 1200}
//     name: "Alice"
//     salary: 1200
//     [[Prototype]]: Person
//         constructor: class Employee
//         sayHello: ƒ sayHello()
//         work: ƒ work()
//         [[Prototype]]: Object
//             constructor: class Person
//             sayHello: ƒ sayHello()
//                 [[Prototype]]: Object
```

## Dynamic inheritance

```js
function MyMixin( cls ){
    return class extends cls{}
}

class SuperPerson extends MyMixin( Person ){

}
```