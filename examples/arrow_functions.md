```js

[1,2,3,4,5].map( function(x){ return x *2 })
// (5) [2, 4, 6, 8, 10]

[1,2,3,4,5].map( (x) => { return x *2 })
// (5) [2, 4, 6, 8, 10]

[1,2,3,4,5].map( (x) => x *2 )
// (5) [2, 4, 6, 8, 10]

[1,2,3,4,5].map( x => x *2 )
// (5) [2, 4, 6, 8, 10]

fn = x => x *2
[1,2,3,4,5].map( fn )
// (5) [2, 4, 6, 8, 10]

```

```js
function Costam(){
    const self = this;

    res = [1,2,3,4].filter(() => {
        // arrow function binds 'this'
        return self === this;
    })
    console.log(res)
}

new Costam()
```

```js

fn = x => console.log(this)

fn.apply({})
// VM12106:1 Window {window: Window, self: Window, document: document, name: "", location: Location, …}

fn.call({})
// VM12106:1 Window {window: Window, self: Window, document: document, name: "", location: Location, …}

fn.bind({})()
// VM12106:1 Window {window: Window, self: Window, document: document, name: "", location: Location, …}

new fn()

// VM12106:7 Uncaught TypeError: fn is not a constructor
//     at <anonymous>:7:1

```