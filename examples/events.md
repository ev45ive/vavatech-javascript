Events
function delegation
dynamic dispatch

```js

class Ctrl{
    handleClick = (e) => console.log('Ctrl click', e)
}
ctrl = new Ctrl()

btn = document.createElement('button')
btn.addEventListener('click', ctrl.handleClick)

btn.click()
// VM13589:2 Ctrl click PointerEvent {isTrusted: false, pointerId: 0, width: 1, height: 1, pressure: 0, …}

```

## event target

```js
new EventTarget()
EventTarget {}[[Prototype]]: EventTarget
addEventListener: ƒ addEventListener()
dispatchEvent: ƒ dispatchEvent()
removeEventListener: ƒ removeEventListener()

t = new EventTarget()
t.addEventListener('test',console.log)
t.dispatchEvent('test')
// VM13987:1 Uncaught TypeError: Failed to execute 'dispatchEvent' on 'EventTarget': parameter 1 is not of type 'Event'.

t.dispatchEvent(new Event('test'))

true
```