```js
searchInput.oninput = e => console.log(e.target.value)

console.log(1)

setTimeout(()=> console.log(2), 0)

Promise.resolve(3).then(console.log)

console.log(4)

console.log('while... 5s')
now = Date.now()
while( now + 5_000 > Date.now()) {} 


console.log(5)
// 1
// 4
// while... 5s
// 5
// 3
// undefined
// a
// ab
// abc
// abcd
// abcde
// 2
```

## Time dilation ;-)

```js
// i = 0
start = Date.now()

handler = setInterval(()=>{
//     i += 1000;

    if(start + 2000 >= Date.now() ){ 
        console.log('alarm!');
        clearInterval(handler)
    }
},1000)

// VM22108:8 alarm!
```