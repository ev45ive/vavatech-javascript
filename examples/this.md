```js

function WhatIsThis(){
    console.log(this); 
}
WhatIsThis() 
// VM19689:2 Window {window: Window, self: Window, document: document, name: "Bob", location: Location, …}

window.WhatIsThis 
// ƒ WhatIsThis(){
//     console.log(this); 
// }

obj = { fn: WhatIsThis }
// {fn: ƒ}

obj.fn() 
// {fn: ƒ}
```

## Apply, call, bind

```js
obj = {palcki:123}
// {palcki: 123}

WhatIsThis.apply(obj/* [a,b,c] */)
// {palcki: 123}
WhatIsThis.call(obj /* a,b,c */)
// {palcki: 123}

obj = {palcki:123}
// {palcki: 123}

fn = WhatIsThis.bind(obj)

fn === WhatIsThis
// false

fn()
// {palcki: 123}

WhatIsThis.name 
// "WhatIsThis"
WhatIsThis.length
// 0
( function(a,b){}).length
// 2
```