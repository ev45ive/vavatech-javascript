```js
null 
// null
undefined
// undefined
typeof undefined
// "undefined"
obj = {} 
// {}
obj.doesntexist
// undefined
options = {}
// options.option == undefined
options.option = 'default'
// "default"
options = { option: null }
options.option == undefined
// options.option = 'default'
// true
typeof null 
"object"
abc 
// VM9756:1 Uncaught ReferenceError: abc is not defined
//     at <anonymous>:1:1

'option' in options
// true

```