
```js
while( $$('tbody')[0].hasChildNodes() )
    $$('tbody')[0].firstChild.remove()

while (harmonogramTbodyEl.children.length)
    harmonogramTbodyEl.firstElementChild.remove()

// or: 
harmonogramTbodyEl.innerText = ''
```

```js

elem.innerText = '<b>Alice</b>' /// "&lt;b&gt;Alice...
elem.innerHTML = '<b>Alice</b>' /// <b>ALice</b>

// Create Element
const tr = document.createElement('tr') // <tr/>
// tr.parent == null
// var td = document.createElement('td')
// tr.append(td)
// tr.class = "nasza-klasa"
// td.innerText = "zawartosc komorki"

tr.innerHTML += '<td>Text</td>' 
tr.innerHTML += `<td>
        <span>${ zmienna }</span>
    </td>
    <td>Text</td>
` 

tbody = document.querySelector('table#harmonogram tbody')
tbody.innerText = '' // Destroy all children
tbody.innerHTML = '' // Destroy all children

tbody.append(tr)
    
```