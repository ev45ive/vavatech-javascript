
## Sync
```js

function calculate(el, callback){
    return el.value * 10
}
result = calculate(limitInputEl)
document.querySelector('h1').innerText = result 

```

## Async

```js
function calculate(el, callback){
    el.onchange = function(event){
        result = event.target.value * 10
        callback(result)
    }
}

function render(result){
    console.log(2)
    document.querySelector('h1').innerText = result 
}

calculate(limitInputEl, render) // async callback
console.log(1)

// 1
// 2

calculate(limitInputEl, console.log )