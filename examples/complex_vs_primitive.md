```js
owoc = {nazwa:'Jabłko'}
// {nazwa: "Jabłko"}
drzewo1 = { owoc: owoc }
// {owoc: {…}}
drzewo2 = { owoc }
// {owoc: {…}}
drzewo1.owoc
// {nazwa: "Jabłko"}
drzewo2.owoc
// {nazwa: "Jabłko"}
owoc.nazwa = 'Dojrzałe jabłko'
"Dojrzałe jabłko"
drzewo1.owoc
// {nazwa: "Dojrzałe jabłko"}
drzewo2.owoc
// {nazwa: "Dojrzałe jabłko"}
delete owoc 
true
drzewo2.owoc
// {nazwa: "Dojrzałe jabłko"}
drzewo1.owoc
// {nazwa: "Dojrzałe jabłko"}
owoc = {nazwa:'Gruszka'}
// {nazwa: "Gruszka"}
drzewo1.owoc
// {nazwa: "Dojrzałe jabłko"}
drzewo2.owoc
// {nazwa: "Dojrzałe jabłko"}
```

```js
dane = { wartosc:1 }
// {wartosc: 1}
cache = dane 
// {wartosc: 1}
dane.wartosc = 2
2
cache
// {wartosc: 2}
dane = { wartosc:'x' }
// {wartosc: "x"}
cache
// {wartosc: 2}
```