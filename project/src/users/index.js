import { UsersListView } from "./UsersView"

import users from './users.json'
import { UserDetailsView } from "./UserDetailsView"
import { User } from "./User"
import { Model } from "./Model"
import { View } from "./View"

class UsersViewModel extends Model {
    active = null
    items = []

    async loadUsers() {
        const res = await fetch('/api/users')
        const data = await res.json()
        this.set('items', data.map(User.fromJSON))
    }
}

class UsersPageView extends View {
    template = /* html */`
    <div class="row">
        <div class="col">
            <div id="users">
                <p>Loading...</p>
            </div>
        </div>
        <div class="col">
            <div id="user-view"></div>
        </div>
    </div>`;

    constructor(el, model) {
        super(el, model)
    }

    render() {
        if (this._initialized) return
        super.render()
        this.usersView = new UsersListView('#users', this.model)
        this.userDetailsView = new UserDetailsView('#user-view', this.model)
    }

}

const pageModel = new UsersViewModel()
window.page = new UsersPageView('#root', pageModel)

pageModel.loadUsers()


