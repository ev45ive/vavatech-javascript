import { View } from './View';

export class UserListItem extends View {
    template = () => /* html */ `
        <div>   
            <strong data-bind="name"></strong>
            <div class="small" data-bind="email"></div>
        </div>`;

    render() {
        if (!this._initialized) {
            this.el.classList.add('list-group-item')
            this.el.dataset.itemId = this.model.id
        }
        super.render()
        this.el.classList.toggle('active', this.model.active || false)
    }
}
