import { get } from 'lodash/fp';

export class View {
    el = document.createElement('div');
    _initialized = false;
    // template = '';

    model = null;

    constructor(el, model) {
        el = typeof el === 'string' ? document.querySelector(el) : el;
        el && (this.el = el);
        model && (this.model = model);
        this.el.dataset.viewClass = this.__proto__.constructor.name
        this.el.__view = this;

        if (this.model) {
            this.model.on('change', this.update)
        }
        if (this.el && this.model) this.update()
    }

    update = () => setTimeout(()=>this.render(), 0)

    destroy() {
        this.model.off('change', this.update)
        this.el.remove()
    }

    setModel(model) {
        this.model = model;
        this.render();
    }


    appendTo(el) {
        el.append(this.el)
        if (!this._initialized) { this.render() }
    }

    render() {
        if (!this._initialized) {
            if (typeof this.template == 'function') {
                this.el.innerHTML = this.template()
            } else {
                this.el.innerHTML = this.template;
            }

            for (let info in this.events) {
                const [selector, eventType] = info.split(' ')
                this.el.addEventListener(eventType, event => {
                    const realTarget = event.target.closest(selector);
                    if (!realTarget) return;

                    const fn = this.events[info]
                    this[fn].call(this, event, realTarget)
                })

            }
            this._initialized = true;
        }
        const binds = this.el.querySelectorAll('[data-bind]');
        binds.forEach(elem => {
            // _.get('user.name', this.model)
            const text = get(elem.dataset.bind, this.model);
            elem.innerText = text;
        });
    }
}
