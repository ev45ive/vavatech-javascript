import { Model } from "./Model";

export class User extends Model {
    id = '';
    name = 'Project manager';
    username = '';
    email = '';
    address = {};

    static fromJSON(data) {
        const user = new User();
        Object.assign(user, data);
        return user;
    }

}
