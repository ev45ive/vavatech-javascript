
import { View } from './View'
import { UserListItem } from './UserListItem'
export class UsersListView extends View {

    template = /* html */`<div class="list-group">Loading...</div>`;

    events = {
        '.list-group-item click': 'onSelectUser',
    }

    onSelectUser = (event, itemEl) => {
        const itemId = itemEl.dataset.itemId
        const item = this.model.items.find(item => item.id == itemId)
        this.model.active?.set('active', false)
        this.model.set('active', item)
        item.set('active', true)
    }

    constructor(el, model) {
        super(el, model)
    }

    items = []

    render() {
        if (this.items === this.model.items) return
        
        super.render()

        this.items?.forEach(item => item.destroy())

        this.model.items.forEach(user => {
            const item = new UserListItem(undefined, user)
            item.appendTo(this.el)
        })
        this.items = this.model.items;
    }
}

