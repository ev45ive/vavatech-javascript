import { set } from "lodash";

// users[0].address.city.anchor // intelisense
export class Model {

    set(path, value) {
        set(this, path, value);
        this.emit('change');
        this.emit('change:' + path);
    }

    _listeners = {};

    emit(eventType) {
        this._listeners[eventType]?.forEach(listener => {
            listener(this);
        });
    }

    on(event, listener) {
        this._listeners[event] = this._listeners[event] || new Map();
        this._listeners[event].set(listener,listener);
    }

    off(event, listener) {
        this._listeners[event]?.delete(listener);
    }

    destroy(){
        this._listeners = {}
    }
}
