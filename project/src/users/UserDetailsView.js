import users from './users.json'
// import get from 'lodash/fp/get'
import { View } from './View'

export class UserDetailsView extends View {
    template =  /* html */`
    <dl>
        <dt>Name:</dt>
        <dd data-bind="active.name"></dd>
        <dt>Email:</dt>
        <dd data-bind="active.email"></dd>
    </dl>
    <button class="btn btn-danger">Cancel</button>
    `
    events = {
        '.btn-danger click':'cancel'
    }

    cancel(){
        this.model.active?.set('active',false)
        this.model.set('active', null)
    }

    constructor(el,model){
        super(el,model)
    }

    render(){
        super.render()

        this.el.classList.toggle('d-none', !this.model.active)
    }
}