const searchInput = document.querySelector('#searchInput')
const searchBtn = document.querySelector('#searchBtn')

searchBtn.addEventListener('click', function () {
    search(searchInput.value)
})

searchInput.addEventListener('keyup', function (event) {
    if (event.key !== 'Enter') return

    search(searchInput.value)
})


function search(query) {
    // const results = 

    // fetchResultsFromServer(query, function(results){
    //     productsList(results)
    // })        
    productsList([])

    fetchResultsFromServer(query, productsList)
    // fetchResultsFromServer(query, console.log)
}

function fetchResultsFromServer(query, callback) {
    fetch('products.json')
    .then( resp =>  resp.json() )
    .then( (data) => {
        callback(data) 
    })
}

// function fetchResultsFromServer(query, callback) {
//     setTimeout(function () {
//         const results = products.filter(function (p) {
//             return p.name.toLocaleLowerCase().includes(query.toLocaleLowerCase())
//         })
//         // productsList(results)
//         callback(results)

//     }, Math.random() * 1000 + 500)
// }