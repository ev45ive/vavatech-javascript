 
let tax = 0.23
let globalDiscount = 0.5
var renderProduct = renderProductDOM

// document.body.children[0].innerText = "Produkty"
const products = [
    {
        id: '123',
        name: 'Placki "Choco\'s"',
        description: 'Placki Czekoladowe',
        category: 'placki',
        price: 9.97,
        promotion: false,
        // discount: undefined
    },
    {
        id: '345',
        name: 'Nalesniki',
        description: 'Nalesniki Truskawkowe',
        category: 'Nalesniki',
        price: 19.97,
        promotion: true,
        review: { rating: 3 }
    },
    {
        id: '456',
        name: 'Pancakes',
        description: 'Pancakes z syropem',
        category: 'Pancakes',
        price: 19.97,
        promotion: true,
        discount: 0.15,
        review: { rating: 3 }
    },
    {
        id: '234',
        name: 'Placki "Banana\'s"',
        description: 'Placki Bananowe',
        category: 'placki',
        price: 8.97,
        promotion: true,
        discount: 0.10
    },
];

const productsContainerEl = document.querySelector('#products')
const listElem = productsContainerEl.querySelector('.list-group')


function productsList(items = products, limit = 3, only_promoted) {
    var displayed = 0

    listElem.innerHTML = ''

    for (let product of items) {

        // Filter promoted
        if (only_promoted !== undefined && only_promoted !== product.promotion) continue;

        // Pagination
        if (++displayed > limit) break;

        // Product Info
        var info = getProductInfo(product)

        renderProduct(info);
    }
}

function renderProductDOM({
    product,
    priceNetto,
    priceBrutto,
    promotion,
    category
}) {

    var div = document.createElement('div')
    div.className = 'list-group-item'
    div.setAttribute('data-product-id', product.id)

    div.innerHTML = /* html */`
        <h4 class="float-end">${priceBrutto.toFixed(2)} PLN</h4>
        <small>[ ${category} ]</small>
        <div><h3> ${product.name} ${promotion} </h3></div>
        <div>${product.description}</div>
        <div>
            <a class="btn btn-primary float-end js-add-to-cart"  href="#" role="button">Add to cart</a>
        </div>
    `
    listElem.append(div)
}
function getProductById(productId) {
    let found = null, index = 0
    do { found = products[index] }
    while (products[index++].id !== productId)

    // for (let product of products) {
    //     if (product.id == productId) {
    //         found = product
    //         break;
    //     }
    // }
    return found
}

function renderProductConsole({
    product,
    priceNetto,
    priceBrutto,
    promotion,
    category
}) {
    var info = `[${category}] ${product.name} (${priceBrutto.toFixed(2)} PLN) ${product.description} ${promotion}`;
    console.log(info);
}

function getProductInfo(product) {
    var priceNetto = getPriceDicounted(product);
    var priceBrutto = getPriceBrutto(priceNetto)
    var promotion = getProductPromotion(product)
    var category = getProductCategory(product);

    return {
        // product:product,
        product,
        priceNetto,
        priceBrutto,
        promotion,
        category,
    }
}

function getProductCategory(product) {
    var category = '';
    switch (product.category) {
        case "placki":
            category += 'Świetne placki';
            break;
        case "Pancakes":
            category += 'Amerykańskie ';
        case "Nalesniki":
            category += 'Pyszne Nalesniki';
            break;
        default:
            category += 'Pozostałe';
    }
    return category;
}

function getProductPromotion(product) {
    return (product.promotion ? 'PROMOTION ' : '');
}

function getPriceDicounted(product) {
    if (product.promotion && product.discount === undefined) {
        var priceNetto = (product.price * (1 - globalDiscount));
    } else if (product.promotion && product.discount !== undefined) {
        var priceNetto = (product.price * (1 - product.discount));
    } else {
        priceNetto = product.price;
    }
    return priceNetto;
}

function getPriceBrutto(priceNetto) {
    return ((priceNetto * (1 + tax)));
}