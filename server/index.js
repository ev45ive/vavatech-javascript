let http = require('http')

const chat = {
    users:[]
}

let server = http.createServer(function (req, res) {
    console.log(req.url);

    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Headers', '*')
    res.setHeader('Content-Type', 'application/json')
    const params = Object.fromEntries((new URLSearchParams(req.url.replace(/^(.*?)\?/, ''))).entries())

    if (req.method === 'POST') {
        var body = '';
        req.on('data', chunk => body += chunk)
        req.on('end', () => {
            res.end(JSON.stringify({ message: 'hello', params, body }))
        })
    } else {
        res.end(JSON.stringify({ message: 'hello', params }))
    }

})

server.listen(8080, () => {
    console.log('Lisetinig ')
})