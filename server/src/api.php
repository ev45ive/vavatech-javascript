<?php
ini_set('log_errors','On');
ini_set('error_log','/dev/stderr');

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');

header('Content-Type: application/json');


// https://symfony.com/doc/current/components/serializer.html
// https://symfony.com/doc/current/setup.html
 
$json =  json_decode(file_get_contents('php://input'), true);


// error_log($json);

echo json_encode($json);

?>