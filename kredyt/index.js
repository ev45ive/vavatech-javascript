
const CreditAmountInput = document.getElementById('creditAmountInput')
const CreditInstalmentsInput = document.querySelector('#creditInstalmentsInput')

const CalculateBtn = document.querySelector('#calculateBtn')

// .innerText = '&gt;b&lt;'
// .innerHTML = '<b>kota</b>'
// .value = ''

const BorrowedAmountOutput = document.getElementById('BorrowedAmountOutput')
const NumberOfInstallmentsOutput = document.getElementById('NumberOfInstallmentsOutput')

const InterestPercentPerYearOutput = document.getElementById('InterestPercentPerYearOutput')

const InstallmentAmountOutput = document.getElementById('InstallmentAmountOutput')
const InterestAmountPerMonthOutput = document.getElementById('InterestAmountPerMonthOutput')
const InterestAmountPerYearOutput = document.getElementById('InterestAmountPerYearOutput')
const TotalAmountOutput = document.getElementById('TotalAmountOutput')

// https://pl.wikipedia.org/wiki/Raty_r%C3%B3wne
// https://kalkulatory.gofin.pl/Kalkulator-kredytowy,12.html


// Config
const credit_interest_percent = 0.10

// Inintial example calculation
creditAmountInput.value = 1000
CreditInstalmentsInput.value = 12
calculate()

// Re-Calculate on change
// CreditAmountInput.oninput = calculate
CreditAmountInput.addEventListener('input', calculate)

// CreditInstalmentsInput.oninput = calculate
CreditInstalmentsInput.addEventListener('input', calculate)

// CalculateBtn.onclick = calculate

// CalculateBtn.addEventListener('click', calculate)

// CalculateBtn.addEventListener('click', console.log)
// CalculateBtn.removeEventListener('click', console.log)

function calculate() {
    // Inputs
    let credit_amount = CreditAmountInput.value * 100
    let credit_installments = CreditInstalmentsInput.value

    BorrowedAmountOutput.innerText = formatMoneyCents(credit_amount)
    NumberOfInstallmentsOutput.innerText = parseInt(credit_installments) + ' miesięcy'
    InterestPercentPerYearOutput.innerText = credit_interest_percent * 100 + ' %'

    const harmonogram = []
    let left_to_pay = credit_amount
    let total_to_pay = 0
    let interest_amount = 0

    for (let index = 0; index < credit_installments; index++) {
        // Procent na miesiąc
        const interest_per_month = credit_interest_percent / 12
        // Kwota na miesiac
        const netto_amount = credit_amount / credit_installments
        // Kwota do spłaty
        // Kwota odsetek
        interest_amount = left_to_pay * interest_per_month
        // Kwota z odsetkami
        brutto_amount = netto_amount + interest_amount
        
        left_to_pay -= netto_amount
        total_to_pay += brutto_amount

        harmonogram.push({
            index,
            interest_per_month,
            interest_amount,
            netto_amount,
            brutto_amount,
            total_to_pay,
            left_to_pay
        })
    }

    NumberOfInstallmentsOutput.innerText = parseInt(credit_installments) + ' miesięcy'
    InstallmentAmountOutput.innerText = formatMoneyCents(brutto_amount)
    InterestAmountPerMonthOutput.innerText = formatMoneyCents(interest_amount)
    TotalAmountOutput.innerText = formatMoneyCents(total_to_pay)

    renderTable(harmonogram);
}

function renderTable(harmonogram) {
    // Find table body
    const harmonogramTbodyEl = document.querySelector('#harmonogram-table > tbody')
    const harmonogramTfootEl = document.querySelector('#harmonogram-table > tfoot')
    // Clear rows
    harmonogramTbodyEl.innerHTML = ''
    let total = 0

    for (let rata of harmonogram) {
        const trElem = document.createElement('tr')

        // '' "" - single line strings,  ` ${..expr..}` - multiline template string
        trElem.innerHTML = `
            <td>${rata.index + 1}</td>
            <td>${formatMoneyCents(rata.brutto_amount)}</td>
            <td>${formatMoneyCents(rata.left_to_pay)}</td>
        `
        harmonogramTbodyEl.append(trElem)
        total += rata.brutto_amount
    }
    harmonogramTfootEl.innerHTML = `
        <td> Razem: </td>
        <td>${formatMoneyCents(total)}</td>
        `
        // <td>${formatMoneyCents(rata.left_to_pay)}</td>

}

function formatMoneyCents(cents) {
    return parseFloat(cents / 100).toFixed(2) + ' zł'
}

