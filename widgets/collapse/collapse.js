


document.addEventListener('click', function (event) {
    if (!event.target.matches('[data-toggle="collapse"]')) return

    const contentElSelector = event.target.getAttribute('href');
    const collapseElem = document.querySelector(contentElSelector)

    collapseElem.classList.toggle('show')

    const isShown = collapseElem.classList.contains('show')
    collapseElem.setAttribute('aria-expanded', isShown)
})


div = document.createElement('div')
div.innerHTML = /* html */`
  <p>
    <a
      class="btn btn-primary"
      data-toggle="collapse"
      href="#content3"
      aria-expanded="false"
      aria-controls="content3"
    >
      Show
    </a>
  </p>
  <div class="collapse show" id="content3">
    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt
    optio officiis cum, sed reiciendis repudiandae excepturi libero
    praesentium totam velit ratione facere illo ducimus odio porro omnis
    nihil, accusantium deserunt.
  </div>
`
document.querySelector('.container .col').append(div)