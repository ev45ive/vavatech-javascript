

document.addEventListener('click', function (event) {
    const buttonEl = event.target;
    // Only collapse buttons
    if (!buttonEl.matches('[data-bs-toggle="collapse"]')) return

    // Get configuration from element data-*
    const contentElSelector = buttonEl.getAttribute('data-bs-target');
    const parentElSelector = collapseElem.getAttribute('data-bs-parent');
    
    // Find related elements
    const collapseElem = document.querySelector(contentElSelector)
    const parentElem = document.querySelector(parentElSelector)

    // Collapse all children of parent element
    parentElem.querySelectorAll('.accordion-button').forEach(function (buttonEl) {
        buttonEl.classList.toggle('collapsed', true)
        const contentElSelector = buttonEl.getAttribute('data-bs-target');
        const collapseElem = document.querySelector(contentElSelector)
        collapseElem.classList.toggle('show',false)
    })
    collapseElem.classList.toggle('show')

    const isShown = collapseElem.classList.contains('show')
    collapseElem.setAttribute('aria-expanded', isShown)
    buttonEl.classList.toggle('collapsed', !isShown)
})
