## Git 
git clone https://bitbucket.org/ev45ive/vavatech-javascript.git vavatech-javascript
cd vavatech-javascript

## Git update
git stash -u 
git pull
npm i 
npm start

## Instalacje
node -v 
v14.17.0

npm -v 
6.14.6

git --version
git version 2.31.1.windows.1

code -v 
1.58.2

chrome://version
Google Chrome	92.0.4515.107

## Debugging
https://developer.chrome.com/docs/devtools/javascript/
https://xdebug.org/docs/install

## Emmet
https://docs.emmet.io/cheat-sheet/


## HTML / CSS / SQL / etc.. in JS
https://marketplace.visualstudio.com/items?itemName=Tobermory.es6-string-html


## MDN
https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement

## CSS selectors
document.querySelector('#someId .someclass')
https://flukeout.github.io/


## VanillaJS vs jQuery (and others..)
http://youmightnotneedjquery.com/
http://vanilla-js.com/



## Functional programing
https://randycoulman.com/blog/categories/thinking-in-ramda/


## Forms
https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects
https://developer.mozilla.org/en-US/docs/Web/API/URL/createObjectURL

## WebSockets
https://stackoverflow.com/questions/52041848/websocket-php-nginx-super-simple
http://socketo.me/docs/hello-world


## Micro macro tasks, animation frame, async etc..
https://www.youtube.com/watch?v=cCOL7MC4Pl0&ab_channel=JSConf


## Testing jasmine
npm install --save-dev jasmine-browser-runner
https://jasmine.github.io/pages/getting_started.html


## Typescript hints
npm i -g typescript
tsc --init --allowJs --rootDirs src --noEmit
npm i @types/jasmine


## ES6 modules


## Webpack 
npm install webpack webpack-cli --save-dev

npx webpack --watch --mode development --devtool inline-source-map

https://webpack.js.org/guides/development/#using-source-maps

https://createapp.dev/webpack
or
npx webpack-cli init

https://webpack.js.org/guides/asset-modules/

https://dev.to/smelukov/webpack-5-asset-modules-2o3h

## json server 

https://jsonplaceholder.typicode.com/
npx json-server https://jsonplaceholder.typicode.com/db
 
 Type s + enter at any time to create a snapshot of the database
  Saved snapshot to db-1628243270571.json

json-server -d 200 --watch db.json


## Frameworki

https://lodash.com/docs/4.17.15#get
https://backbonejs.org/#Model
https://backbonejs.org/#View

http://rivetsjs.com/docs/guide/
https://alpinejs.dev/directives/data 
https://vuejs.org/v2/guide/